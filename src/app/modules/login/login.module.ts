import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { SigninComponent } from './sign-in/sign-in.component';
import { LoginRoutingModule } from './login-routing.module';

@NgModule({
  declarations: [LoginComponent, SigninComponent],
  imports: [CommonModule, LoginRoutingModule],
})
export class LoginModule {}
