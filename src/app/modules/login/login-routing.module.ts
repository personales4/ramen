import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';

/* const routes: Routes = [
  {
    path: '',
    component: SigninComponent, // SOLO UN ROUTER
    children: [
        {
            path: '',
            component: LoginComponent
        },
        {
            path: 'registry',
            component: RegistryComponent
        },
        {
            path: 'verification',
            component: VerificationCodeComponent
        },
        {
          path: 'recover',
          component: PasswordRecoverComponent
      },
        { path: '**', redirectTo: '' }
    ]
  }
];
 */

const routes: Routes = [{ path: '', component: LoginComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginRoutingModule {}
