import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginModule } from './modules/login/login.module';
import { LandingComponent } from './modules/landing/landing.component';
import { NotFoundComponent } from './modules/not-found/not-found.component';
import { ClinicHistoryComponent } from './modules/clinic-history/clinic-history.component';

const routes: Routes = [
  { path: 'landing', component: LandingComponent },
  {
    path: 'login',
    loadChildren: () =>
      import('./modules/login/login.module').then((m) => m.LoginModule),
  },
  { path: 'clinic-history', component: ClinicHistoryComponent },

  { path: 'not-found', component: NotFoundComponent },
  { path: '', redirectTo: '/landing', pathMatch: 'full' },
  { path: '**', redirectTo: 'not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
