import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClinicHistoryComponent } from './modules/clinic-history/clinic-history.component';
import { LandingComponent } from './modules/landing/landing.component';
import { LoginModule } from './modules/login/login.module';
import { HeaderComponent } from './shared-modules/header/header.component';
import { FooterComponent } from './shared-modules/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LandingComponent,
    ClinicHistoryComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, LoginModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
